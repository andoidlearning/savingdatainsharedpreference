package com.example.savingdatainsharedpreferences

import android.content.Context
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.CheckBox
import android.widget.EditText

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val edtName : EditText = findViewById(R.id.edtName)
        val edtAge : EditText = findViewById(R.id.edtAge)
        val chkBox : CheckBox = findViewById(R.id.chkAdult)
        val btnSave : Button = findViewById(R.id.btnSave)
        val btnLoad : Button = findViewById(R.id.btnLoad)

        val sharedPref = getSharedPreferences("myPref", Context.MODE_PRIVATE)
        val editor = sharedPref.edit()

        btnSave.setOnClickListener {
            val name = edtName.text.toString()
            val age = edtAge.text.toString().toInt()
            val isAdult = chkBox.isChecked

            editor.apply{
                putString("name", name)
                putInt("age", age)
                putBoolean("isAdult", isAdult)
                apply() // commit() use karu shakto pn ekade ha main thread block krto
                // apply() he kam asynchronusly krto ha main thread block karat nahi
            }
        }

        btnLoad.setOnClickListener {
            val name = sharedPref.getString("name", null)
            val age = sharedPref.getInt("age", 0)
            val isAdult = sharedPref.getBoolean("isAdult",false)

            edtName.setText(name)
            edtAge.setText(age.toString())
            chkBox.isChecked = isAdult
        }

    }
}